package pl.car.carauth.api.users.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.car.carauth.api.users.converter.UserConverter;
import pl.car.carauth.api.users.dto.UserDto;
import pl.car.carauth.api.users.repository.UserRepository;
import pl.car.carauth.api.users.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserConverter userConverter;

    @Override
    public List<UserDto> findAll() {
        return userRepository.findAll().stream().map(userConverter::toUserDto).collect(Collectors.toList());
    }
}
