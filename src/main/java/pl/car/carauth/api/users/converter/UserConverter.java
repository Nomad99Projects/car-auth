package pl.car.carauth.api.users.converter;

import org.springframework.stereotype.Component;
import pl.car.carauth.api.users.dto.UserDto;
import pl.car.carauth.api.users.model.UserEntity;

@Component
public class UserConverter {

    public UserDto toUserDto(UserEntity entity) {
        UserDto userDto = new UserDto();
        userDto.setId(entity.getId());
        userDto.setRole(entity.getRole());
        userDto.setUsername(entity.getUsername());

        return userDto;
    }
}
