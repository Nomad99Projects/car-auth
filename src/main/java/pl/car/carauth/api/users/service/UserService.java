package pl.car.carauth.api.users.service;

import pl.car.carauth.api.users.dto.UserDto;

import java.util.List;

public interface UserService {

    List<UserDto> findAll();
}
