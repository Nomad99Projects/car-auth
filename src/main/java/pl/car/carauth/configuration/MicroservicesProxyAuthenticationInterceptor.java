package pl.car.carauth.configuration;

import com.google.common.collect.Lists;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.RequiredArgsConstructor;
import org.h2.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import pl.car.carauth.filter.UserContextHolder;
import pl.car.carauth.security.TokenProvider;

import static org.hibernate.internal.util.collections.CollectionHelper.isNotEmpty;
import static pl.car.carauth.configuration.ApplicationConstants.AUTHORIZATION;
import static pl.car.carauth.configuration.ApplicationConstants.BEARER;

@Component
@RequiredArgsConstructor
@SuppressWarnings("Duplicates")
public class MicroservicesProxyAuthenticationInterceptor implements RequestInterceptor {

    public static final String SYSTEM_ROLE = "ROLE_SYSTEM";
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "12345";

    @Lazy
    private final TokenProvider tokenProvider;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        String correlationId = UserContextHolder.getContext().getCorrelationId();
        if (!StringUtils.isNullOrEmpty(correlationId)) {
            requestTemplate.header(ApplicationConstants.CORRELATION_ID, correlationId);
        }

        String authToken = UserContextHolder.getContext().getAuthToken();
        if (!StringUtils.isNullOrEmpty(authToken)) {
            requestTemplate.header(AUTHORIZATION, BEARER + authToken);
            return;
        }
        if (!StringUtils.isNullOrEmpty(UserContextHolder.getContext().getAuthToken())) {
            requestTemplate.header(AUTHORIZATION, UserContextHolder.getContext().getAuthToken());
            return;
        }
        if(!requestTemplateHasToken(requestTemplate)) {
            requestTemplate.header(AUTHORIZATION, generateToken());
        }

    }

    private boolean requestTemplateHasToken(RequestTemplate requestTemplate) {
        return isNotEmpty(requestTemplate.headers().get(AUTHORIZATION));
    }

    private String generateToken() {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD,
                Lists.newArrayList(new SimpleGrantedAuthority(SYSTEM_ROLE)));
        return BEARER + tokenProvider.generateJwtToken(usernamePasswordAuthenticationToken);
    }

}
