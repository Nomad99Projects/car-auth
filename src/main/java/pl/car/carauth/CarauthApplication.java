package pl.car.carauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class CarauthApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarauthApplication.class, args);
    }
}
