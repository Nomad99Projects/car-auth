package pl.car.carauth.filter;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import pl.car.carauth.security.JWTProperties;

import java.io.IOException;

import static pl.car.carauth.configuration.ApplicationConstants.CORRELATION_ID;

@Component
@SuppressWarnings("Duplicates")
@RequiredArgsConstructor
public class UserContextInterceptor implements ClientHttpRequestInterceptor {

    private final JWTProperties jwtProperties;

    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes, ClientHttpRequestExecution clientHttpRequestExecution) throws IOException {
        HttpHeaders headers = httpRequest.getHeaders();
        headers.add(CORRELATION_ID, UserContextHolder.getContext().getCorrelationId());
        headers.add(jwtProperties.getHeader(), UserContextHolder.getContext().getAuthToken());

        return clientHttpRequestExecution.execute(httpRequest, bytes);
    }
}
