package pl.car.carauth;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import pl.car.carauth.api.users.model.UserEntity;
import pl.car.carauth.api.users.repository.UserRepository;


@Component
@RequiredArgsConstructor
public class InitUsersCommandLineRunner implements CommandLineRunner {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void run(String... strings) throws Exception {

        UserEntity userEntity1 = new UserEntity();
        userEntity1.setUsername("user");
        userEntity1.setPassword(bCryptPasswordEncoder.encode("12345"));
        userEntity1.setRole("USER");

        UserEntity userEntity2 = new UserEntity();
        userEntity2.setUsername("admin");
        userEntity2.setPassword(bCryptPasswordEncoder.encode("12345"));
        userEntity2.setRole("ADMIN");

        userRepository.save(userEntity1);
        userRepository.save(userEntity2);
    }
}
