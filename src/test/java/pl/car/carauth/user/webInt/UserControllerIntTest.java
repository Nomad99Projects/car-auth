package pl.car.carauth.user.webInt;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import pl.car.carauth.api.users.model.UserEntity;
import pl.car.carauth.api.users.repository.UserRepository;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerIntTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Test
    @WithMockUser(roles = "USER")
    public void userShouldNotBeAuthorizedToGetUsers() throws Exception {
        mockMvc.perform(get("/api/user")).andExpect(status().isForbidden()).andDo(print());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void adminShouldGetUserList() throws Exception {
        mockMvc.perform(get("/api/user")).andExpect(status().isOk()).andDo(print());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldGetUsersList() throws Exception {
        UserEntity userEntity1 = new UserEntity();
        userEntity1.setUsername("Username1");
        userEntity1.setPassword("Password1");
        userEntity1.setRole("ADMIN");

        UserEntity userEntity2 = new UserEntity();
        userEntity2.setUsername("Username2");
        userEntity2.setPassword("Password2");
        userEntity2.setRole("USER");

        userRepository.save(userEntity1);
        userRepository.save(userEntity2);

        mockMvc.perform(get("/api/user"))
                .andExpect(status().isOk())
                .andExpect(content().string(Matchers.not(Matchers.isEmptyString())))
                .andExpect(jsonPath("$.[*].username").value(Matchers.hasItems(userEntity1.getUsername(), userEntity2.getUsername())))
                .andExpect(jsonPath("$.[*].password").value(Matchers.not(Matchers.hasItems(userEntity1.getPassword(), userEntity2.getPassword()))))
                .andDo(print());

    }
}
