package pl.car.carauth.user.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.car.carauth.api.users.model.UserEntity;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    public void shouldPersistAndBePresent() {
        UserEntity userEntity = new UserEntity();
        userEntity.setUsername("Username");
        userEntity.setPassword("Password");
        userEntity.setRole("ADMIN");

        UserEntity savedUser = testEntityManager.persistFlushFind(userEntity);

        Assertions.assertThat(savedUser).isNotNull();
    }
}
